// pages/Savegoods_Finance_goback/Savegoods_Finance_goback.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lock:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  calling(e) {
    wx.makePhoneCall({
      phoneNumber: '8888888', //此号码并非真实电话号码，仅用于测试
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
    })

  },
  showmodal(e){
    console.log(e)
    if(this.data.lock){
      wx.showModal({
        title: '已锁仓禁止出货',
        confirmText:'知道了',
        showCancel:false,
        content: '该客户已被锁仓，目前无法出货请“监管仓人员”做好监管！',
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }else{
      wx.showModal({
        title: '出货数量',
        content: '请确认2018-05-19 出货该规格货品数量为 3000 ？如您单品用完每日出度今日不能再出货。',
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
   
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})